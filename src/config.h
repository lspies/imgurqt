/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef CONFIG_H
#define CONFIG_H

#include <QString>

/****************************
 *
 * Here you need to specify the client_id and the client_secret you obtained by registering this application with imgur.com
 *
 * If you use these scripts inside your own application, please be fair and obtain your own client_id and client_secret (it's free and only takes 5 minutes)
 *
 * You can either enter the client_id/client_secret below, or you can specify a web URL where the script can find this info. The info behind the URL has to be provided in the form of the following two lines:
 *
 * client_id=[the_id]
 * client_secret=[the_secret]
 *
 *****************************/

const QString CLIENT_ID = "8ad72edfcd8934b";
const QString CLIENT_SECRET = "0d8e10754070beda15f1ec4f0b2852f0e911b787";

// Specify either the two above or the one below... See info above!

const QString CLIENT_ID_SECRET_URL = "";


#endif // CONFIG_H
