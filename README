 ImgurQt - Copyright (C) 2017-2021, Lukas Spies (Lukas@photoqt.org),
 License: GPLv2 (or later) -- Website: https://gitlab.com/luspi/imgurqt
--------------------------------------------------------------------

 ImgurQt implements imgur.com's APIv3 for uploading and deleting
 images to and from imgur.com.


 STANDALONE APPLICATION
------------------------
 ImgurQt works as a stand-alone application. After compiling, you can
 call it with various command-line flags. Simply run it without any
 flags or pass -h/--help to see all the available options. You can
 authenticate with user accounts on imgur.com, and upload images either
 to an account or anonymously. After successful completion of an upload,
 ImgurQt will return the image URL and a hash that allows direct deletion
 of an image. Simply open the provided URL or pass the hash to ImgurQt
 using the --delete flag to delete an image.


 DEPENDENCIES
--------------
 ImgurQt does not have many dependencies. It should be sufficient to have
 qt5-base (name might vary depending on your distribution). For compilation,
 cmake is required.


 BUILD
-------
 ImgurQt is built by simply running:

 1) cd build
 2) cmake ..
 3) make

 This will create an executable in the build dirctory called 'imgurqt'.
 ImgurQt does currently not provide a way to install it to system directories.


 IN YOUR OWN APPLICATION
-------------------------
 All the functionality of authenticating/uploading/deleting images from
 imgur.com is contained in the files located in the subdirectory src/.
 You only need to do the following steps:

 1) Include imgur.h in your own project

 2) Adjust the client_id and client_secret in config.h to the ones you
    obtain yourself (please be fair, get your own for your own project)

 3) Add imgur.cpp and simplecrypt.cpp to the list of files to be compiled

 4) Start using it

 To see a pratical example of how to use ImgurQt, check out the file
 main.cpp. It demonstrates with plenty of source code comments how to
 implement this functionality.

 NOTE:
 The authentication data is not stored in plaintext, but using some simple
 encryption (nothing fancy, and not guaranteed to be safe). The key for
 this encryption is based on the hostname of the machine.


 FINAL REMARKS
---------------
 ImgurQt was born out of the desire to implement this functionality in PhotoQt
 (http://photoqt.org). During development, it seemed like a logical step to
 publish them as a separate small project. BUGS can be reported as issues
 on its GitLab page - https://gitlab.com/luspi/imgurqt - or by sending an
 email to Lukas@photoqt.org.

